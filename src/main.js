import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import VueCordova from 'vue-cordova'
// import cordovaPlugins from './assets/js/cordovaPlugins.js'

//NutUI
import NutUI from '@nutui/nutui';
import '@nutui/nutui/dist/nutui.css';
NutUI.install(Vue);

//导入vant
import { Checkbox, CheckboxGroup,Popup,Dialog,PullRefresh,ActionSheet,Toast,SwipeCell,Cell } from 'vant';
Vue.use(Checkbox).use(CheckboxGroup).use(Popup).use(Dialog).use(PullRefresh).use(ActionSheet).use(Toast).use(SwipeCell).use(Cell);

//vue-datetime
import 'vue-datetime/dist/vue-datetime.css'
import { Datetime } from 'vue-datetime';
Vue.component('datetime', Datetime);

// 导入font-awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'

library.add(fas, far, fab)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)


//mintui
// import { MessageBox } from 'mint-ui'
// import 'mint-ui/lib/style.css'
// Vue.use(MessageBox);

//引入basic CSS
//引入CSS
import '@/src/css/common.css'
import '@/src/assets/iconfont/font/ali-font.css'
import '@/src/assets/iconfont/icon/iconfont.css'

Vue.use(ElementUI);
// Vue.use(VueCordova);
// Vue.config.$cordovaPlugins=cordovaPlugins;


//引入API
import api from './js/api.js'
Vue.prototype.api = api;
//引入jquery
import $ from 'jquery'
Vue.prototype.$=$;
Vue.config.productionTip = false;

new Vue({
    router: router,
    store: store,
    render: h => h(App),
}).$mount('#app')
