import Vue from 'vue'
import Vuex from 'vuex'
import 'es6-promise/auto'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      id: "loading",
      userName: "loading",
      avatarUrl: "loading",
    },
    tabbarShow: false,
  },
  setters: {

  },
  getters: {
    getTabbarShow(state) {
      return state.tabbarShow
    },
  },
  mutations: {
    updateUser(state, user) {
      state.user = user;
    },
    updateTabbarShow(state, payload) {
      state.tabbarShow = payload
    }
  },
  actions: {
  },
  modules: {
  }
})
