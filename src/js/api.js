import httpRequest from './httpRequest'
var htr = httpRequest.httpRequest;

//example from caocao
import fileApi from './Together-apis/file'
var uploadFile = fileApi.uploadFile;
var getFileUrl = fileApi.getFileUrl;
import userApi from './Together-apis/user'
var register = userApi.register;
var updateUser = userApi.updateUser;
import projectApi from './Together-apis/project'

//导入kunyu apis
import kunyuApi from './Together-apis/kunyu-api'
var GetFileList=kunyuApi.GetFileList;
var PostFile=kunyuApi.PostFile;
var GetFile=kunyuApi.GetFile;
var DeleteFile=kunyuApi.DeleteFile;

import yunboApi from './Together-apis/yunbo-api'

//给你的api命名
var kunyu_example=kunyuApi.kunyu_example;
var yunbo_get=yunboApi.yunbo_get;
var yunbo_post=yunboApi.yunbo_post;
var yunbo_emailGetID=yunboApi.yunbo_emailGetID;
var DeleteUser=yunboApi.DeleteUser;

import longyuApi from './Together-apis/longyu-api'
var GetProjectMember=longyuApi.GetProjectMember;
var GetUserProject=longyuApi.GetUserProject;
var GetProjectInformation=longyuApi.GetProjectInformation;
var joinProject=longyuApi.joinProject;
var deleteProject=longyuApi.deletePtoject;

import taskApi from './Together-apis/siqi-api'
var GetTasks=taskApi.GetTasks;
var GetSetTasks=taskApi.GetSetTasks;
var PostTaskUser=taskApi.PostTaskUser;
var PostTaskTimeStart=taskApi.PostTaskTimeStart;
var PostTaskTimeEnd=taskApi.PostTaskTimeEnd;
var PostTaskTitle=taskApi.PostTaskTitle;
var PostTaskDescription=taskApi.PostTaskDescription;
var PostTaskState=taskApi.PostTaskState;
var DeleteTask=taskApi.DeleteTask;
var AddTask=taskApi.AddTask;
var ChangeState=taskApi.ChangeState;


export default {
    htr,
    //example from caocao
    userApi,
    projectApi,
    uploadFile,
    getFileUrl,
    register,
    updateUser,
    //下面开始export你们的
    
    yunbo_get,
    yunbo_post,
    yunbo_emailGetID,
    DeleteUser,

    kunyu_example,  //前端调用：this.api.kunyu_example
    GetFileList,
    PostFile,
    GetFile,
    DeleteFile,

    GetUserProject,
    GetProjectMember,
    GetProjectInformation,
    joinProject,
    deleteProject,

    GetTasks,
    GetSetTasks,
    PostTaskUser,
    PostTaskTimeStart,
    PostTaskTimeEnd,
    PostTaskTitle,
    PostTaskDescription,
    PostTaskState,
    DeleteTask,
    AddTask,
    ChangeState,
}
