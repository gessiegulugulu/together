import httpRequest from '../httpRequest'
var htr = httpRequest.httpRequest;


var addProject = function (project) {
    return htr('/api/v1/project', 'post', project);
}

var joinProject = function (project_id,id) {
    return htr('/api/v1/user/'+id+'/project/'+project_id, 'post',{});
}

export default {
    addProject,
    joinProject
}