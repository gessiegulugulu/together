import httpRequest from '../httpRequest'
var htr = httpRequest.httpRequest;

var yunbo_get = function (id) {
    //htr返回一个Promise，前端要取得它请求得到的值需要加await
    //参数：1.url，2.method，3.parameters
    return htr('/api/v1//project/'+id+'/user', 'get', {});
}

var yunbo_post = function (id,taskid,userProject) {
    //htr返回一个Promise，前端要取得它请求得到的值需要加await
    //参数：1.url，2.method，3.parameters
    let xhr=htr('/api/v1/user/'+id+'/project/'+taskid, 'post', userProject);
    return xhr;
}

var yunbo_emailGetID = function (userEmail) {
    return htr('/api/v1/user/email/'+userEmail, 'get');
}

var DeleteUser = function (userid,projectid) {
    return htr('/api/v1/user/'+userid+'/project/'+projectid, 'delete');
}

export default{
    yunbo_get,
    yunbo_post,
    yunbo_emailGetID,
    DeleteUser,
}