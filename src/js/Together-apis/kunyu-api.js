import httpRequest from '../httpRequest'
var htr = httpRequest.httpRequest;

//example
var kunyu_example = function () {
    //htr返回一个Promise，前端要取得它请求得到的值需要加await
    //参数：1.url，2.method，3.parameters
    return htr('/api/v1/user/1', 'get', {});
}
var GetFileList = function (project_id) {
    return htr('/api/v1/project/' + project_id + '/file', 'get', {});
}

var PostFile = function (project_id,{ file_name , file_url }) {
    htr('/api/v1/project/' + project_id + '/file', 'post',{"name":file_name,"url":file_url});
    return;
}

var GetFile = function (project_id, file_name) {
    return htr('/api/v1/project/' + project_id + '/file/'+ file_name, 'get', {});
}

var DeleteFile = function (project_id, file_name) {
    return htr('/api/v1/project/' + project_id + '/file/'+ file_name, 'delete', {});
}

export default{
    kunyu_example,
    GetFileList,
    PostFile,
    GetFile,
    DeleteFile,
}
