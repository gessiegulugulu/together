import Vue from 'vue'
import Router from 'vue-router'
import VueCordova from 'vue-cordova'
import HelloWorld from '@/src/components/HelloWorld'
import Test from '@/src/components/test/Test'
import Homepage from '@/src/components/Homepage'
import Task from '@/src/components/Task'
import Articlepage from '@/src/components/Articlepage'
import FileList from '@/src/components/FileList'
import Member from "./components/Member";
import Login from "./components/Login"
import Register from "./components/Register"
import Taskset from '@/src/components/TaskSet'
import PersonInfo from '@/src/components/page_personal/PersonInfo'
import Camera from '@/src/components/Camera'


Vue.use(VueCordova)
Vue.use(Router)

export default new Router({
    mode: 'hash',
    routes: [
        {
            path: '/hello',
            name: 'HelloWorld',
            component: HelloWorld,
        },
        {
            path: '/task/:id',
            name: 'Task',
            component: Task,
        },
        {
            path: '/',
            name: 'Homepage',
            component: Homepage,
            // path: '/',
            // name: 'Task',
            // component: Task,
        },
        {
            path: '/article',
            name: 'Articlepage',
            component:Articlepage ,
        },
        {
            path: '/filelist/:id',
            name: 'FileList',
            component:FileList ,
        },
        {
            path: '/member/:id',
            name: 'Member',
            component:Member ,
        },
        {
            path: '/login',
            name: 'Login',
            component:Login ,
        },
        {
            path: '/register',
            name: 'Register',
            component:Register ,
        },
        {
            path:'/personinfo/:id',
            name:'PersonInfo',
            component:PersonInfo,
        },
        {
            path:'/taskset/:id',
            name:'Taskset',
            component:Taskset,
        },
        {
            path:'/test',
            name:'Test',
            component:Test,
        },
        {
            path:'/camera',
            name:'Camera',
            component:Camera,
        },
    ]
})